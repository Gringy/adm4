<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${month.name} ${month.year}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <style>
        .month-checker {
            float: left;
        }

        .header {
            padding-left: auto;
            padding-right: auto;
            font-size: 1.5rem;
            float: left;
        }

        td, th {
            margin: auto;
        }

        .not-current-month {
            color: #888;
        }

        .current-month {
            color: black;
            padding: 6px;
            border: solid 1px #888;
        }

        .filled {
            text-decoration: underline;
        }

        .today {
            border-color: #c88;
        }
    </style>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<div class="container">
    <div class="month-checker btn-group">
        <a href="?year=${month.prevYear}&month=${month.prevMonth}" class="btn btn-secondary">&lt;</a>
        <a href="?year=${month.nextYear}&month=${month.nextMonth}" class="btn btn-secondary">&gt;</a>
    </div>
    <div class="header">${month.name} ${month.year}</div>
    <table class="table">
        <thead>
        <tr>
            <c:forEach var="name" items="${month.dayNames}">
                <th>${name}</th>
            </c:forEach>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="week" items="${month.weeks}">
            <tr>
                <c:forEach var="day" items="${week.days}">
                    <c:set var="dayStyle" value="${''}"/>
                    <c:if test="${!day.currentMonth}">
                        <c:set var="dayStyle" value="${dayStyle += 'not-current-month '}"/>
                    </c:if>
                    <c:if test="${day.currentDay}">
                        <c:set var="dayStyle" value="${dayStyle += 'today '}"/>
                    </c:if>
                    <c:if test="${day.currentMonth}">
                        <c:if test="${day.filled}">
                            <c:set var="dayStyle" value="${dayStyle += 'current-month filled '}"/>
                        </c:if>
                        <c:if test="${!day.filled}">
                            <c:set var="dayStyle" value="${dayStyle += 'current-month '}"/>
                        </c:if>
                    </c:if>
                    <td>
                        <c:if test="${day.currentMonth}">
                            <a class="${dayStyle}"
                               href="day?day=${day.dayOfMonth}&month=${month.month}&year=${month.year}">
                                    ${day.dayOfMonth}
                            </a>
                        </c:if>
                        <c:if test="${!day.currentMonth}">
                            <div class="${dayStyle}">${day.dayOfMonth}</div>
                        </c:if>
                    </td>
                </c:forEach>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
