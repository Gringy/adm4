<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${day.dayOfMonth} ${day.monthName} ${day.year}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <style>
        .record-container {
            display: grid;
            grid-template-columns: min-content auto;
            grid-template-rows: auto;
            grid-template-areas: "time content" "edit content" " .   content";
            padding-top: 5pt;
            padding-left: 0;
            padding-right: 0;
        }

        .time {
            color: #6c757d;
            border-color: #6c757d;
            grid-area: time;
            text-align: center;
        }

        .edit {
            font-size: 1.5rem;
            grid-area: edit;
            text-align: center;
        }

        .content {
            font-size: 0.9rem;
            grid-area: content;
            margin-left: 2px;
        }

        .head-container {
            display: grid;
            grid-template-columns: min-content auto;
            grid-template-rows: auto;
            grid-template-areas: "number dow" "number month-year";
        }

        .head-container * {
            line-height: 1;
        }

        .number {
            grid-area: number;
            font-weight: bold;
            font-size: 2.5rem;
        }

        .dow {
            grid-area: dow;
            font-size: 1.1rem;
        }

        .month-year {
            grid-area: month-year;
            font-size: 1.1rem;
        }

        .thing-container {
            display: grid;
            grid-template-columns: min-content auto max-content;
            grid-template-rows: auto;
            grid-template-areas: "state label change";
            padding-left: 0;
            padding-right: 0;
        }

        .state {
            grid-area: state;
            font-size: 1.2rem;
            align-items: center;
        }

        .label {
            grid-area: label;
        }

        .change {
            grid-area: change;
        }

        textarea.time {
            resize: none;
            font-size: 0.9rem;
            padding: 0;
        }

        input[name="label"] {
            height: 100%;
        }

        textarea.content {
            padding: 0;
            resize: none;
            overflow: hidden;
            min-height: 50px;
        }

        select.form-control.change {
            width: fit-content;
            height: 100%;
        }

        .state, .number, .time, .edit {
            width: 5rem;
        }
    </style>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<div class="container-fluid">
    <!-- header -->
    <div class="col-12 record-container">
        <a href="./?month=${day.month}&year=${day.year}" class="btn btn-outline-secondary time">◀️</a>
        <div class="content">
            <div class="head-container">
                <div class="number">${day.dayOfMonth}</div>
                <div class="dow">${day.dayOfWeek}</div>
                <div class="month-year">${day.monthName} ${day.year}</div>
            </div>
        </div>
    </div>
    <!-- records list -->
    <c:forEach var="record" items="${day.records}">
        <div class="col-12 record-container">
            <div class="card time">${record.recordTime}</div>
            <div class="edit btn-group-vertical">
                <a href="edit?id=${record.id}" class="btn btn-outline-secondary">️📝</a>
                <a href="del?id=${record.id}" class="btn btn-outline-secondary">🔥</a>
            </div>
            <div class="card content">
                <c:forEach var="line" items="${record.content.stringsHTML}">
                    ${line}
                </c:forEach>
            </div>
        </div>
    </c:forEach>
    <!-- record adding area -->
    <form class="col-12 record-container" action="add" method="POST">
        <textarea class="time form-control" name="time" cols="15" rows="1">${day.newRecordTime}</textarea>
        <button type="submit" class="edit btn btn-outline-secondary">+</button>
        <textarea onkeyup="auto_grow(this)" class="content form-control" name="text"></textarea>
        <input type="hidden" name="day" value="${day.dayOfMonth}">
        <input type="hidden" name="month" value="${day.month}">
        <input type="hidden" name="year" value="${day.year}">
    </form>
    <!-- things list -->
    <c:forEach var="thing" items="${day.things}">
        <div class="col-12 thing-container">
            <div class="card state">
                <c:if test="${thing.type == 'DONE'}">
                    <c:if test="${thing.finished}">✅</c:if>
                    <c:if test="${!thing.finished}">❎</c:if>
                </c:if>
                <c:if test="${thing.type == 'MANY'}">${thing.counter}</c:if>
            </div>
            <div class="card label" id="label${thing.id}">${thing.label}</div>
            <div class="change">
                <div class="btn-group">
                    <c:if test="${thing.type == 'DONE'}">
                        <c:if test="${thing.finished}">
                            <a class="btn btn-primary" href="undone?id=${thing.id}">Undone</a>
                        </c:if>
                        <c:if test="${!thing.finished}">
                            <a class="btn btn-primary" href="done?id=${thing.id}">Done</a>
                        </c:if>
                    </c:if>
                    <c:if test="${thing.type == 'MANY'}">
                        <button type="button" onclick="setupChangePopup(${thing.id})" class="btn btn-primary"
                                data-toggle="modal" data-target="#changeCounterModal">
                            Change
                        </button>
                    </c:if>
                    <button type="button" onclick="setupPopup(${thing.id})"
                            class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu">
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#thingEditModal">
                            Edit
                        </button>
                        <a class="dropdown-item" href="today?id=${thing.id}">Move to today</a>
                        <a class="dropdown-item" href="delThing?id=${thing.id}">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
    <!-- thing adding area -->
    <form class="col-12 thing-container" action="addThing" method="post">
        <button type="submit" class="state btn btn-outline-secondary">+</button>
        <input type="text" class="label form-control" name="label">
        <select class="form-control change" name="type">
            <option>DONE</option>
            <option>MANY</option>
        </select>
        <input type="hidden" name="day" value="${day.dayOfMonth}">
        <input type="hidden" name="month" value="${day.month}">
        <input type="hidden" name="year" value="${day.year}">
    </form>
    <!-- thing editing popup -->
    <form method="post" action="editThing" class="modal fade" id="thingEditModal" tabindex="-1" role="dialog"
          aria-labelledby="thingEditModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="thingEditModalLabel">Edit label</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="label" id="label">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
        <input type="hidden" name="id" id="postEditId">
    </form>
    <!-- change thing's counter modal popup -->
    <form method="post" action="changeCounter" class="modal fade" id="changeCounterModal" tabindex="-1" role="dialog"
          aria-labelledby="changeCounterModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="changeCounterModalLabel">Change thing counter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Set</div>
                        </div>
                        <input type="text" class="form-control" name="set">
                    </div>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Increase</div>
                        </div>
                        <input type="text" class="form-control" name="inc">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
        <input type="hidden" name="id" id="changeCounterId">
    </form>
    <script>
        function auto_grow(element) {
            //element.style.height = "5px";
            element.style.height = (element.scrollHeight) + "px";
        }

        function setupPopup(id) {
            var label = document.getElementById("label" + id);
            var postEditIdInput = document.getElementById("postEditId");
            var labelElement = document.getElementById("label");
            postEditIdInput.value = "" + id;
            labelElement.value = label.innerText;
        }

        function setupChangePopup(id) {
            var changeCounterId = document.getElementById("changeCounterId");
            changeCounterId.value = "" + id;
        }
    </script>
</div>
</body>
</html>
