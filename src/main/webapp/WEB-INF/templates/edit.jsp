<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit record</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <style>
        textarea#ta {
            width: 100%;
            min-height: 80vh;
        }

        textarea#tags {
            width: 100%;
        }
    </style>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<div class="container">
    <form method="POST" action="edit">
        <div class="col-12">
            <c:if test="${record.id != 0}">
                <h4>Edit #${record.id}</h4>
            </c:if>
            <c:if test="${record.id == null}">
                <h4> Edit new record</h4>
            </c:if>
            <h5>Created: ${record.date} ${record.time}</h5>
            <h5>Modified: ${record.modifiedDate}</h5>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
        <div class="col-12">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Description</div>
                </div>
                <input type="text" class="form-control" value="${record.description}" name="desc">
            </div>
        </div>
        <div class="col-12">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text">Tags</div>
                </div>
                <input type="text" class="form-control" value="${record.tags}" name="tags">
            </div>
        </div>
        <div class="col-12">
            <textarea class="form-control" name="content" id="ta">${record.content}</textarea>
        </div>
        <c:if test="${record.id != 0}">
            <input type="hidden" name="id" value="${record.id}">
        </c:if>
    </form>
</div>
</body>
</html>