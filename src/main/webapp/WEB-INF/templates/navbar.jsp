<%@ page contentType="text/html;charset=UTF-8" %>
<style>
    body {
        padding-top: 60px;
    }
</style>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="./">Calendar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="list">List</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="stats">Stats</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="logout">Logout</a>
            </li>
        </ul>
    </div>
</nav>
