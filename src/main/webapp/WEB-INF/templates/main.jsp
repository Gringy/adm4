<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ADM4</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
    <style>
        .record-action-buttons {
            float: right;
        }
        .icon {
            font-size: 14pt;
        }
    </style>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<div class="container">
    <c:forEach items="${records}" var="record">
        <div class="card" style="width:100%;">
            <div class="card-body">
                <div class="btn-group-vertical record-action-buttons">
                    <a class="btn btn-outline-success icon" href="show?id=${record.id}">
                        👀
                    </a>
                    <a class="btn btn-outline-primary icon" href="edit?id=${record.id}">
                        ✍
                    </a>
                    <a class="btn btn-outline-danger icon" href="del?id=${record.id}">
                        🔥
                    </a>
                </div>
                <h5 class="card-title m-1">#${record.id} ${record.description}</h5>
                <p class="card-subtitle text-muted m-1" style="font-size:10pt;">Created: ${record.date}</p>
                <p class="card-subtitle text-muted m-1" style="font-size:10pt;">Modifyed: ${record.modifiedDate}</p>
                <div class="card-text">
                    <c:if test="${record.tags != null}">
                        <c:forEach items="${record.tags}" var="tag">
                            <a href="?tag=${tag}">#${tag}</a>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
        </div>
    </c:forEach>
    <div class="btn-group">
        <c:forEach var="pagen" items="${pages}">
            <c:if test="${pagen != page}">
                <a class="btn btn-primary" href="?page=${pagen}">${pagen}</a>
            </c:if>
            <c:if test="${pagen == page}">
                <a class="btn btn-success" href="?page=${pagen}">${pagen}</a>
            </c:if>
        </c:forEach>
    </div>
</div>
</body>
</html>
