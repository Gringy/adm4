<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Stats</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
            crossorigin="anonymous"></script>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<div class="container">
    <table class="table table-sm table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>$$</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${simpleStats}" var="entry">
            <tr>
                <th>${entry.key}</th>
                <th>${entry.value}</th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <table class="table table-sm table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>Time</th>
            <th>Remote IP</th>
            <th>Session ID</th>
            <th>Username</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${auths}" var="auth">
            <c:if test="${auth.authenticated}">
                <tr class="table-success">
            </c:if>
            <c:if test="${!auth.authenticated}">
                <tr>
            </c:if>
            <td>${auth.time}</td>
            <td>${auth.remoteAddress}</td>
            <td>${auth.sessionId}</td>
            <td>${auth.username}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
