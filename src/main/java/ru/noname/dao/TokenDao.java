package ru.noname.dao;

import org.springframework.stereotype.Repository;
import ru.noname.data.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Repository
public class TokenDao {
    private List<Token> tokens;

    public TokenDao() {
        tokens = new ArrayList<>();
        tokens.add(new Token("00000000000000000000000000000000", false));
        tokens.add(new Token(generateToken(), true));
        tokens.add(new Token(generateToken(), false));
    }

    public List<Token> getTokens() {
        return Collections.unmodifiableList(tokens);
    }

    private String generateToken() {
        Random random = new Random();
        byte[] bytes = new byte[32];
        random.nextBytes(bytes);
        StringBuilder builder = new StringBuilder();
        String mapping = "0123456789abcdef";
        for (byte b : bytes) {
            builder.append(mapping.charAt((b >> 4) & 0xF));
            builder.append(mapping.charAt(b & 0xF));
        }
        return builder.toString();
    }

    public Token addToken() {
        Token token = new Token(generateToken(), false);
        tokens.add(token);
        return token;
    }

    public boolean validate(String toValidate) {
        for (Token token : tokens)
            if (token.getToken().equals(toValidate))
                return !token.isDeleted();
        return false;
    }
}
