package ru.noname.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.noname.data.Record;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class RecordDao {
    private JdbcTemplate jdbcTemplate;

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE;
    private final DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_LOCAL_TIME;

    public List<Record> getRecordsPage(RecordsOrder order, int count, int page) {
        String query = "SELECT r.RecordId, r.Description, r.Content, r.Modified, r.RDate, r.RTime, t.Name FROM Record r "
                + "INNER JOIN Tag t ON r.TagId = t.TagId";
        switch (order) {
            case NO_ORDER:
            default:
                break;
            case BY_CREATED:
                query += " ORDER BY r.RDate, r.RTime DESC";
                break;
            case BY_MODIFIED:
                query += " ORDER BY r.Modified DESC";
                break;
            case BY_ID:
                query += " ORDER BY r.RecordId DESC";
                break;
        }
        if (count > 0 && page > 0) {
            int offset = (page - 1) * count;
            query += " LIMIT " + count + " OFFSET " + offset;
        }
        return jdbcTemplate.query(query, new RecordMapper());
    }

    public int getTotalPages(int pageSize) {
        String query = "SELECT COUNT(*) FROM Record";
        Integer count = jdbcTemplate.queryForObject(query, Integer.class);
        if (count == null)
            return 0;
        int pages = count / pageSize;
        int away = count % pageSize;
        return pages + (away != 0 ? 1 : 0);
    }

    public Record getRecordById(int id) {
        final String query = "SELECT * FROM Record r WHERE r.RecordId=?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, new RecordMapper());
    }

    public void updateRecord(Record record) {
        String query = "UPDATE Record r SET r.Description=?, r.Content=?, r.TagId=(SELECT t.TagId FROM Tag t WHERE t.Name = ?) WHERE r.RecordId=?";
        jdbcTemplate.update(query,
                record.getDescription(),
                record.getContent(),
                record.getTags(),
                record.getId());
    }

    public void createRecord(Record record) {
        String query = "INSERT INTO Record(Description, RDate, RTime, TagId, Content) VALUES(?, ?, ?, (SELECT t.TagId FROM Tag t WHERE t.Name = ?), ?)";
        jdbcTemplate.update(query,
                record.getDescription(),
                record.getDate().format(dateFormatter),
                record.getTime().format(timeFormatter),
                record.getTags(),
                record.getContent());
    }

    public void deleteRecordById(int id) {
        String query = "DELETE FROM Record WHERE RecordId=?";
        jdbcTemplate.update(query, id);
    }

    public List<Record> getDay(int day, int month, int year) {
        String query = "SELECT * FROM Record r WHERE DAYOFMONTH(r.RDate) = ? AND MONTH(r.RDate) = ? AND YEAR(r.RDate) = ? ORDER BY r.RTime ASC";
        return jdbcTemplate.query(query, new Object[]{day, month, year}, new RecordMapper());
    }

    private static class DayOfMonthMapper implements RowMapper<Integer> {
        @Override
        public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
            LocalDate rowDate = LocalDate.parse(resultSet.getString("RDate"));
            return rowDate.getDayOfMonth();
        }
    }

    public Set<Integer> getMonthFilled(int month, int year) {
        String query = "SELECT r.RDate FROM Record r WHERE MONTH(r.RDate) = ? AND YEAR(r.RDate) = ? GROUP BY r.RDate";
        List<Integer> list = jdbcTemplate.query(query, new Object[]{month, year}, new DayOfMonthMapper());
        return new HashSet<>(list);
    }

    public int getSizeOfRecords() {
        String query = "SELECT SUM(LENGTH(r.Content)) FROM Record r";
        Integer result = jdbcTemplate.queryForObject(query, Integer.class);
        return result != null ? result : 0;
    }

    public int getRecordsCount() {
        String query = "SELECT COUNT(*) FROM Record";
        Integer result = jdbcTemplate.queryForObject(query, Integer.class);
        return result != null ? result : 0;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
