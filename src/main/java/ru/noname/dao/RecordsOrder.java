package ru.noname.dao;

public enum RecordsOrder {
    BY_CREATED,
    BY_MODIFIED,
    BY_ID,
    NO_ORDER
}
