package ru.noname.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.noname.data.Thing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class ThingMapper implements RowMapper<Thing> {
    @Override
    public Thing mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        Thing thing = new Thing();
        thing.setId(resultSet.getInt("ThingId"));
//        thing.setDay(resultSet.getInt("Day"));
//        thing.setMonth(resultSet.getInt("Month"));
//        thing.setYear(resultSet.getInt("Year"));
        thing.setDate(LocalDate.parse(resultSet.getString("Date")));
        thing.setLabel(resultSet.getString("Label"));
        thing.setType(Thing.Type.valueOf(resultSet.getString("Type")));
        thing.setCounter(resultSet.getInt("Counter"));
        return thing;
    }
}
