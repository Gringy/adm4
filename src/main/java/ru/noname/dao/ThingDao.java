package ru.noname.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.noname.data.Thing;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository
public class ThingDao {
    private JdbcTemplate jdbcTemplate;

    public void createThing(Thing thing) {
        String query = "INSERT INTO Thing(`Date`, Label, Type, Counter) VALUES(?, ?, ?, ?)";
        jdbcTemplate.update(query,
                thing.getDateString(),
                thing.getLabel(),
                thing.getType().name(),
                thing.getCounter());
    }

    public Thing getThingById(int id) {
        String query = "SELECT * FROM Thing t WHERE t.ThingId=?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, new ThingMapper());
    }

    public List<Thing> getDayThings(int day, int month, int year) {
        String query = "SELECT * FROM Thing t WHERE DAYOFMONTH(t.Date)=? AND MONTH(t.Date)=? AND YEAR(t.Date)=? ORDER BY t.ThingId ASC";
        return jdbcTemplate.query(query,
                new Object[]{day, month, year},
                new ThingMapper());
    }

    public void setThingCounter(int id, int counter) {
        String query = "UPDATE Thing t SET t.Counter=? WHERE t.ThingId=?";
        jdbcTemplate.update(query, counter, id);
    }

    public void setThingLabel(int id, String label) {
        String query = "UPDATE Thing t SET t.Label=? WHERE t.ThingId=?";
        jdbcTemplate.update(query, label, id);
    }

    public void deleteThing(int id) {
        String query = "DELETE FROM Thing t WHERE t.ThingId=?";
        jdbcTemplate.update(query, id);
    }

    public void moveToToday(int id) {
        String query = "UPDATE Thing t SET t.Date=CURRENT_DATE() WHERE t.ThingId=?";
        jdbcTemplate.update(query, id);
    }

    private static class DayOfMonthMapper implements RowMapper<Integer> {
        @Override
        public Integer mapRow(ResultSet resultSet, int i) throws SQLException {
            LocalDate rowDate = LocalDate.parse(resultSet.getString("Date"));
            return rowDate.getDayOfMonth();
        }
    }

    public Set<Integer> getMonthFilled(int month, int year) {
        String query = "SELECT t.Date FROM Thing t WHERE MONTH(t.Date) = ? AND YEAR(t.Date) = ? GROUP BY t.Date";
        List<Integer> list = jdbcTemplate.query(query, new Object[]{month, year}, new DayOfMonthMapper());
        return new HashSet<>(list);
    }

    public int getThingsCount() {
        String query = "SELECT COUNT(*) FROM Thing";
        Integer result = jdbcTemplate.queryForObject(query, Integer.class);
        return result != null ? result : 0;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
}
