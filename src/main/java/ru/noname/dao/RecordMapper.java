package ru.noname.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.noname.data.Record;
import ru.noname.data.RecordHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;

public class RecordMapper implements RowMapper<Record> {
    @Override
    public Record mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        RecordHelper record = new RecordHelper();
        record.setId(resultSet.getInt("RecordId"))
                .setDescription(resultSet.getString("Description"))
                .setDate(LocalDate.parse(resultSet.getString("RDate")))
                .setTime(LocalTime.parse(resultSet.getString("RTime")))
                .setModified(resultSet.getTimestamp("Modified").getTime() / 1000)
                .setContent(resultSet.getString("Content"));
        // TODO: map tag
        //.setTags(resultSet.getString("Name"));
        return record.getRecord();
    }
}
