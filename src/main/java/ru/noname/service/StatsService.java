package ru.noname.service;

import com.sun.management.OperatingSystemMXBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.noname.dao.RecordDao;
import ru.noname.dao.ThingDao;

import java.lang.management.ManagementFactory;
import java.util.Map;
import java.util.TreeMap;

@Service
public class StatsService {
    private RecordDao recordDao;
    private ThingDao thingDao;

    private TelegramService telegramService;

    public Map<String, String> getSimpleStats() {
        Map<String, String> result = new TreeMap<>();

        result.put("Total records", Integer.toString(recordDao.getRecordsCount()));
        result.put("Total chars", Integer.toString(recordDao.getSizeOfRecords()));
        result.put("Total things", Integer.toString(thingDao.getThingsCount()));

        OperatingSystemMXBean mx = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

        double KiB = 1024.0;
        double MiB = KiB * 1024.0;

        long freePhysicalMemorySize = mx.getCommittedVirtualMemorySize() - mx.getFreeSwapSpaceSize();
        long totalPhysicalMemorySize = mx.getTotalPhysicalMemorySize();
        result.put(
                "Memory free/total",
                String.format(
                        "%.1fMB/%.1fMB (%.2f%%)",
                        freePhysicalMemorySize / MiB,
                        totalPhysicalMemorySize / MiB,
                        ((double) freePhysicalMemorySize / totalPhysicalMemorySize * 100.0))
        );

        result.put("Cpu load", Double.toString(mx.getSystemCpuLoad()));

        result.put("Telegram received", Integer.toString(telegramService.getReceivedMessages()));
        result.put("Telegram sent: ", Integer.toString(telegramService.getSentMessages()));

        return result;
    }

    @Autowired
    public void setRecordDao(RecordDao recordDao) {
        this.recordDao = recordDao;
    }

    @Autowired
    public void setThingDao(ThingDao thingDao) {
        this.thingDao = thingDao;
    }

    @Autowired
    public void setTelegramService(TelegramService telegramService) {
        this.telegramService = telegramService;
    }
}
