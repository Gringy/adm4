package ru.noname.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class AuthService {
    private static final Logger log = LogManager.getLogger(AuthService.class);
    private TelegramService telegramService;

    private List<AuthDetails> auths;

    public static class AuthDetails {
        boolean authenticated;

        String remoteAddress;
        String sessionId;
        String username;
        String password;

        LocalDateTime time;

        AuthDetails(AbstractAuthenticationEvent event) {
            time = LocalDateTime.now();

            Authentication auth = event.getAuthentication();
            WebAuthenticationDetails details = (WebAuthenticationDetails) auth.getDetails();
            this.remoteAddress = details.getRemoteAddress();
            this.sessionId = details.getSessionId();

            if (auth.getPrincipal() instanceof User) {
                authenticated = true;
                User user = (User) auth.getPrincipal();
                this.username = user.getUsername();
                this.password = user.getPassword();
            } else if (auth.getPrincipal() instanceof String) {
                authenticated = false;
            }
        }

        public boolean isAuthenticated() {
            return authenticated;
        }

        public String getRemoteAddress() {
            return remoteAddress;
        }

        public String getSessionId() {
            return sessionId;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        public LocalDateTime getTime() {
            return time;
        }

        @Override
        public String toString() {
            return "AuthDetails{" +
                    "authenticated=" + authenticated +
                    ", remoteAddress='" + remoteAddress + '\'' +
                    ", sessionId='" + sessionId + '\'' +
                    ", username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }

    @Component
    static class SuccessAuthListener implements ApplicationListener<AuthenticationSuccessEvent> {
        private AuthService authService;

        @Override
        public void onApplicationEvent(AuthenticationSuccessEvent e) {
            authService.addAuth(new AuthDetails(e));
        }

        @Autowired
        public void setAuthService(AuthService authService) {
            this.authService = authService;
        }
    }

    @Component
    static class FailureAuthListener implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
        private AuthService authService;

        @Override
        public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent e) {

            authService.addAuth(new AuthDetails(e));
        }

        @Autowired
        public void setAuthService(AuthService authService) {
            this.authService = authService;
        }
    }

    public AuthService() {
        this.auths = new ArrayList<>();
    }

    public List<AuthDetails> getAuthDetails() {
        return Collections.unmodifiableList(auths);
    }

    @Autowired
    public void setTelegramService(TelegramService telegramService) {
        this.telegramService = telegramService;
    }

    private void addAuth(AuthDetails details) {
        if (details.isAuthenticated()) {
            log.info("success auth: " + details);
            telegramService.sendMessage("Authentication: " + details);
        } else {
            log.info("fail auth: " + details);
            telegramService.sendMessage("Failed authentication: " + details);
        }
        auths.add(details);
    }
}
