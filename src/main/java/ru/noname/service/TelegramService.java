package ru.noname.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.noname.data.Message;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TelegramService {
    private static final Logger log = LogManager.getLogger(TelegramService.class);
    private StatsService statsService;

    private String token;
    private int userId;
    private boolean enableUpdates;

    private RestTemplate rest;
    private HttpHeaders headers;
    private ObjectMapper mapper;

    private int offset;
    private int receivedMessages = 0;
    private int sentMessages = 0;

    public TelegramService() {
        rest = new RestTemplate();
        mapper = new ObjectMapper();

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        offset = 0;
    }

    private String getUrl(String apiMethod) {
        return "https://api.telegram.org/bot" + token + "/" + apiMethod;
    }

    private String postQuery(String apiMethod, String json) {
        HttpEntity<String> requestEntity = new HttpEntity<>(json, headers);
        ResponseEntity<String> responseEntity = rest.exchange(getUrl(apiMethod), HttpMethod.POST, requestEntity, String.class);
        return responseEntity.getBody();
    }

    private List<Message> getMessages() {
        String json = (offset == 0)
                ? "{}"
                : "{\"offset\":" + (offset + 1) + "}";
        String response = postQuery("getUpdates", json);

        List<Message> ret = new ArrayList<>();

        synchronized (this) {
            try {
                JsonNode node = mapper.readTree(response);
                if (!node.get("ok").asBoolean()) {
                    log.error("telegram API returns ok: false");
                    return null;
                }
                JsonNode result = node.get("result");
                for (JsonNode update : result) {
                    receivedMessages++;
                    int update_id = update.get("update_id").asInt();
                    if (update_id > offset)
                        offset = update_id;
                    JsonNode message = update.get("message");
                    int chat_id = message.get("chat").get("id").asInt();
                    String username = message.get("from").get("username").asText();
                    if (message.has("text")) {
                        String text = message.get("text").asText();
                        ret.add(new Message(chat_id, text, username, chat_id == userId));
                    }
                }
            } catch (IOException e) {
                System.err.println(e);
                return null;
            }
        }
        return ret;
    }

    private void sendMessage(int id, String message) {
        synchronized (this) {
            sentMessages++;
        }
        try {
            ObjectNode node = mapper.createObjectNode();
            node.put("chat_id", id);
            node.put("text", message);

            String json = mapper.writeValueAsString(node);

            postQuery("sendMessage", json);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
    }

    public void sendMessage(String message) {
        sendMessage(userId, message);
    }

    @Scheduled(fixedRate = 10000L)
    public void receiveMessages() {
        if (!enableUpdates)
            return;
        List<Message> messages = getMessages();

        if (messages == null) {
            log.error("error on try to receive messages from telegram");
        }

        for (Message message : messages) {
            if (message.isTrusted()) {
                log.info("message from master: " + message.getText());
                masterMessage(message.getText());
            } else {
                log.info("message from " + message.getUsername() + "[" + message.getChatId() + "]: " + message.getText());
                sendMessage(message.getChatId(), "What you want?");
            }
        }
    }

    @PostConstruct
    private void startupMessage() {
        try {
            InetAddress local = InetAddress.getLocalHost();
            String hostName = local.getHostName();
            String hostAddress = local.getHostAddress();
            sendMessage("I'm started: " + hostName + "@" + hostAddress);
        } catch (UnknownHostException e) {
            log.error(e);
        }
    }

    private void masterMessage(String message) {

        switch (message) {
            case "/stats": {
                Map<String, String> stats = statsService.getSimpleStats();
                StringBuilder builder = new StringBuilder();
                for (Map.Entry<String, String> entry : stats.entrySet())
                    builder.append(entry.getKey() + ": " + entry.getValue() + "\n");
                sendMessage(builder.toString());
                break;
            }
            default:
                sendMessage("oll korrect");
        }
    }

    public synchronized int getReceivedMessages() {
        return receivedMessages;
    }

    public synchronized int getSentMessages() {
        return sentMessages;
    }

    @Autowired
    public void setStatsService(StatsService statsService) {
        this.statsService = statsService;
    }

    @Autowired
    public void setToken(@Value("${telegram.token}") String token) {
        this.token = token;
    }

    @Autowired
    public void setUserId(@Value("${telegram.user}") int userId) {
        this.userId = userId;
    }

    @Autowired
    public void setEnableUpdates(@Value("${telegram.enable}") boolean enableUpdates) {
        this.enableUpdates = enableUpdates;
    }
}
