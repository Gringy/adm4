package ru.noname.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.noname.dao.RecordDao;
import ru.noname.dao.ThingDao;

import java.text.DateFormatSymbols;
import java.time.*;
import java.util.*;

@Service
public class CalendarService {
    private final static int FIRST_DAY_OF_WEEK = Calendar.MONDAY;

    public static class DayModel {
        DayModel(int dayOfMonth, boolean filled, boolean currentMonth, boolean currentDay) {
            this.dayOfMonth = dayOfMonth;
            this.filled = filled;
            this.currentMonth = currentMonth;
            this.currentDay = currentDay;
        }

        int dayOfMonth;
        boolean filled;
        boolean currentMonth;
        boolean currentDay;

        public int getDayOfMonth() {
            return dayOfMonth;
        }

        public boolean isFilled() {
            return filled;
        }

        public boolean isCurrentMonth() {
            return currentMonth;
        }

        public boolean isCurrentDay() {
            return currentDay;
        }
    }

    public static class WeekModel {
        WeekModel() {
            days = new ArrayList<>();
        }

        List<DayModel> days;

        void addDay(DayModel day) {
            days.add(day);
        }

        public List<DayModel> getDays() {
            return days;
        }
    }

    public static class MonthModel {
        MonthModel() {
            weeks = new ArrayList<>();
            dayNames = new ArrayList<>();
        }

        String name;
        int month;
        int year;

        int prevMonth, prevYear;
        int nextMonth, nextYear;

        List<WeekModel> weeks;

        // names of day of week
        List<String> dayNames;

        void addWeek(WeekModel week) {
            weeks.add(week);
        }

        public String getName() {
            return name;
        }

        public int getMonth() {
            return month;
        }

        public int getYear() {
            return year;
        }

        public List<WeekModel> getWeeks() {
            return weeks;
        }

        public List<String> getDayNames() {
            return dayNames;
        }

        public int getPrevMonth() {
            return prevMonth;
        }

        public int getPrevYear() {
            return prevYear;
        }

        public int getNextMonth() {
            return nextMonth;
        }

        public int getNextYear() {
            return nextYear;
        }
    }

    private RecordDao recordDao;
    private ThingDao thingDao;

    public MonthModel getMonth(int month, int year) {
        if (month <= 0 || month > 12)
            throw new IllegalArgumentException("month must be in [1..12");

        MonthModel monthModel = new MonthModel();
        monthModel.dayNames = getDayOfWeekNames();

        Set<Integer> monthFilling = recordDao.getMonthFilled(month, year);
        monthFilling.addAll(thingDao.getMonthFilled(month, year));

        WeekModel currentWeek = new WeekModel();
        LocalDate currentDay = LocalDate.of(year, month, 1).with(DayOfWeek.MONDAY);

        ZonedDateTime today = Clock.systemDefaultZone().instant().atZone(ZoneId.of("Europe/Moscow"));
        int todayDay = today.getDayOfMonth();
        int todayMonth = today.getMonthValue();
        int todayYear = today.getYear();

        // add days in first week in previous month
        while (currentDay.getMonthValue() != month) {
            currentWeek.addDay(new DayModel(currentDay.getDayOfMonth(), false, false, false));
            currentDay = currentDay.plusDays(1);
        }

        // add days in current month
        while (currentDay.getMonthValue() == month) {
            if (currentWeek.days.size() == 7) {
                monthModel.addWeek(currentWeek);
                currentWeek = new WeekModel();
            }

            boolean isFilled = monthFilling.contains(currentDay.getDayOfMonth());
            boolean isToday = currentDay.getDayOfMonth() == todayDay && currentDay.getMonthValue() == todayMonth && currentDay.getYear() == todayYear;
            currentWeek.addDay(new DayModel(currentDay.getDayOfMonth(), isFilled, true, isToday));
            currentDay = currentDay.plusDays(1);
        }

        // add last days
        while (currentWeek.days.size() != 7) {
            currentWeek.addDay(new DayModel(currentDay.getDayOfMonth(), false, false, false));
            currentDay = currentDay.plusDays(1);
        }

        monthModel.addWeek(currentWeek);

        monthModel.name = DayService.monthName(month);
        monthModel.month = month;
        monthModel.year = year;

        final LocalDate prevMonthDate = LocalDate.of(year, month, 1).minusMonths(1);
        monthModel.prevYear = prevMonthDate.getYear();
        monthModel.prevMonth = prevMonthDate.getMonthValue();

        final LocalDate nextMonthDate = LocalDate.of(year, month, 1).plusMonths(1);
        monthModel.nextYear = nextMonthDate.getYear();
        monthModel.nextMonth = nextMonthDate.getMonthValue();

        return monthModel;
    }

    private List<String> getDayOfWeekNames() {
        ArrayList<String> names = new ArrayList<>();
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale("RU"));
        String[] weekdayNames = symbols.getShortWeekdays();

        for (int aDay = FIRST_DAY_OF_WEEK; aDay < weekdayNames.length; aDay++)
            names.add(weekdayNames[aDay]);
        for (int aDay = 1; aDay < FIRST_DAY_OF_WEEK; aDay++)
            names.add(weekdayNames[aDay]);
        return names;
    }

    @Autowired
    public void setRecordDao(RecordDao recordDao) {
        this.recordDao = recordDao;
    }

    @Autowired
    public void setThingDao(ThingDao thingDao) {
        this.thingDao = thingDao;
    }
}
