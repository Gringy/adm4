package ru.noname.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.noname.dao.RecordDao;
import ru.noname.dao.ThingDao;
import ru.noname.data.Record;
import ru.noname.data.RecordHelper;
import ru.noname.data.Thing;
import ru.noname.util.ContentRenderer;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DayService {
    private RecordDao recordDao;
    private ThingDao thingDao;

    public static class DayModel {
        LocalDate date;
        String monthName;
        String dayOfWeek;
        List<RecordModel> records;
        List<ThingModel> things;
        String newRecordTime;

        public int getDayOfMonth() {
            return date.getDayOfMonth();
        }

        public String getMonthName() {
            return monthName;
        }

        public String getDayOfWeek() {
            return dayOfWeek;
        }

        public int getYear() {
            return date.getYear();
        }

        public List<RecordModel> getRecords() {
            return records;
        }

        public List<ThingModel> getThings() {
            return things;
        }

        public int getMonth() {
            return date.getMonthValue();
        }

        public String getNewRecordTime() {
            return newRecordTime;
        }
    }

    public static class RecordModel {
        int id;
        String recordTime;
        ContentRenderer content;

        public int getId() {
            return id;
        }

        public String getRecordTime() {
            return recordTime;
        }

        public ContentRenderer getContent() {
            return content;
        }
    }

    public static class ThingModel {
        int id;
        String label;
        String type;
        boolean finished;
        int counter;

        public int getId() {
            return id;
        }

        public String getLabel() {
            return label;
        }

        public String getType() {
            return type;
        }

        public boolean isFinished() {
            return finished;
        }

        public int getCounter() {
            return counter;
        }
    }

    public DayModel getDay(int day, int month, int year) {
        try {
            List<Record> records = recordDao.getDay(day, month, year);
            List<RecordModel> recordModels = new ArrayList<>(records.size());
            for (Record record : records) {
                RecordModel model = new RecordModel();
                RecordHelper helper = new RecordHelper(record);
                model.recordTime = helper.getTimeString();
                model.id = record.getId();
                model.content = helper.getRenderer();
                recordModels.add(model);
            }
            List<Thing> things = thingDao.getDayThings(day, month, year);
            List<ThingModel> thingModels = new ArrayList<>(things.size());
            for (Thing thing : things) {
                ThingModel model = new ThingModel();
                model.id = thing.getId();
                model.label = thing.getLabel();
                model.counter = thing.getCounter();
                switch (thing.getType()) {
                    case DONE:
                        model.type = "DONE";
                        model.finished = thing.getCounter() != 0;
                        break;
                    case MANY:
                        model.type = "MANY";
                        break;
                    default:
                        model.type = "UNKNOWN";
                }
                thingModels.add(model);
            }
            DayModel model = new DayModel();
            model.date = LocalDate.of(year, month, day);
            model.records = recordModels;
            model.things = thingModels;
            model.monthName = monthName(month);
            model.dayOfWeek = dayOfWeekName(model.date.getDayOfWeek().getValue());
            model.newRecordTime = getTime(ZonedDateTime.now(ZoneId.of("Europe/Moscow")).toLocalTime());
            return model;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String getTime(LocalTime time) {
        return String.format("%02d:%02d", time.getHour(), time.getMinute());
    }

    static String monthName(int month) {
        if (month <= 0 || month > 12)
            throw new IllegalArgumentException("month must be in [1..12]");
        String[] names = {"Январь", "Февраль", "Март",
                "Апрель", "Май", "Июнь",
                "Июль", "Август", "Сентябрь",
                "Октябрь", "Ноябрь", "Декабрь"};
        return names[month - 1];
    }

    private static String dayOfWeekName(int dayOfWeek) {
        String[] names = {"Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"};
        return names[dayOfWeek - 1];
    }

    @Autowired
    public void setRecordDao(RecordDao recordDao) {
        this.recordDao = recordDao;
    }

    @Autowired
    public void setThingDao(ThingDao thingDao) {
        this.thingDao = thingDao;
    }
}
