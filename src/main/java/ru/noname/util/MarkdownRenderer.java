package ru.noname.util;

import com.github.rjeschke.txtmark.Processor;

import java.util.ArrayList;
import java.util.List;

public class MarkdownRenderer implements ContentRenderer {
    private String source;

    public MarkdownRenderer(String source) {
        this.source = source;
    }

    @Override
    public List<String> getStringsHTML() {
        String result = Processor.process(source);
        List<String> toReturn = new ArrayList<>();
        toReturn.add(result);
        return toReturn;
    }
}
