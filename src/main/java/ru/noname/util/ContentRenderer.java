package ru.noname.util;

import java.util.List;

public interface ContentRenderer {
    List<String> getStringsHTML();
}
