package ru.noname.util;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class AddressReplaceServletFilter implements Filter {
    static class Handler implements InvocationHandler {
        private final HttpServletRequest original;
        private String remoteAddress;

        public Handler(HttpServletRequest original, String remoteAddress) {
            this.original = original;
            this.remoteAddress = remoteAddress;
        }

        public Object invoke(Object proxy, Method method, Object[] args)
                throws IllegalAccessException, IllegalArgumentException,
                InvocationTargetException {

            if (method.getName().equals("getRemoteAddr"))
                return remoteAddress;
            else
                return method.invoke(original, args);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String address = httpRequest.getHeader("x-forwarded-for");
        if (address == null)
            address = httpRequest.getRemoteAddr();

        Handler handler = new Handler(httpRequest, address);
        HttpServletRequest newRequest = (HttpServletRequest) Proxy.newProxyInstance(HttpServletRequest.class.getClassLoader(), new Class[]{HttpServletRequest.class}, handler);
        filterChain.doFilter(newRequest, response);
    }

    @Override
    public void destroy() {
    }
}
