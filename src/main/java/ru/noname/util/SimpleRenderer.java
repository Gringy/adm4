package ru.noname.util;

import java.util.ArrayList;
import java.util.List;

public class SimpleRenderer implements ContentRenderer {
    private String content;

    public SimpleRenderer(String content) {
        this.content = content;
    }

    public List<String> getStringsHTML() {
        String[] lines = content.split("\\r?\\n");
        List<String> result = new ArrayList<>();
        for (String line : lines) {
            boolean bold = false;
            if (line.length() != 0 && line.charAt(0) == '#') {
                bold = true;
                line = line.replaceFirst("#", "");
            }
            result.add((bold ? "<b>" : "") + line + (bold ? "</b>" : "") + "<br>");
        }
        return result;
    }
}
