package ru.noname.data;

import ru.noname.util.ContentRenderer;
import ru.noname.util.MarkdownRenderer;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class RecordHelper {
    private Record record;

    public RecordHelper() {
        record = new Record();
    }

    public RecordHelper(Record record) {
        this.record = record;
    }

    public Record getRecord() {
        return record;
    }

    public RecordHelper setRecord(Record record) {
        this.record = record;
        return this;
    }

    public ContentRenderer getRenderer() {
        return new MarkdownRenderer(record.getContent());
    }

    public List<String> getTagList() {
        String tags = record.getTags();
        if (tags == null || tags.equals(""))
            return Collections.emptyList();
        String[] tagsArray = tags.split(" ");
        if (tagsArray.length == 0 || tagsArray[0].equals(""))
            return Collections.emptyList();
        return Arrays.asList(tagsArray);
    }

    public RecordHelper setTagList(List<String> tagList) {
        if (tagList.size() == 0) {
            record.setTags("");
            return this;
        }
        record.setTags(String.join(" ", tagList));
        return this;
    }

    public RecordHelper setTagArray(String[] tagArray) {
        if (tagArray == null || tagArray.length == 0) {
            record.setTags("");
            return this;
        }
        record.setTags(String.join(" ", tagArray));
        return this;
    }

    public LocalDate getDate() {
        return record.getDate();
    }

    public RecordHelper setDate(LocalDate date) {
        record.setDate(date);
        return this;
    }

    public String getDateString() {
        return record.getDate().format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public LocalTime getTime() {
        return record.getTime();
    }

    public RecordHelper setTime(LocalTime time) {
        record.setTime(time);
        return this;
    }

    public String getTimeString() {
        return record.getTime().format(DateTimeFormatter.ISO_LOCAL_TIME);
    }

    public Date getModifiedDate() {
        return new Date(record.getModified() * 1000);
    }

    public RecordHelper setModifiedDate(Date modified) {
        record.setModified(modified.getTime() / 1000);
        return this;
    }

    // delegate methods to record in builder mode

    public int getId() {
        return record.getId();
    }

    public RecordHelper setId(int id) {
        record.setId(id);
        return this;
    }

    public String getDescription() {
        return record.getDescription();
    }

    public RecordHelper setDescription(String description) {
        record.setDescription(description);
        return this;
    }

    public String getTags() {
        return record.getTags();
    }

    public RecordHelper setTags(String tags) {
        record.setTags(tags);
        return this;
    }

    public long getModified() {
        return record.getModified();
    }

    public RecordHelper setModified(long timestamp) {
        record.setModified(timestamp);
        return this;
    }

    public String getContent() {
        return record.getContent();
    }

    public RecordHelper setContent(String content) {
        record.setContent(content);
        return this;
    }
}
