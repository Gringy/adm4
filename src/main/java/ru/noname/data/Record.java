package ru.noname.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class Record implements Serializable {
    private int id;
    private String description;
    private String tags;
    //private long created;
    private LocalDate date;
    private LocalTime time;
    private long modified;
    private String content;

    public Record() {
        //
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

//    public long getCreated() {
//        return created;
//    }
//
//    public void setCreated(long timestamp) {
//        created = timestamp;
//    }


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long timestamp) {
        modified = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Record{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", tags='" + tags + '\'' +
                ", date=" + date +
                ", time=" + time +
                ", modified=" + modified +
                ", content='" + content + '\'' +
                '}';
    }
}
