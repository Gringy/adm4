package ru.noname.data;

public class Message {
    private int chatId;
    private String text;
    private String username;
    private boolean trusted;

    public Message(int chatId, String text, String username, boolean trusted) {
        this.chatId = chatId;
        this.text = text;
        this.username = username;
        this.trusted = trusted;
    }

    public int getChatId() {
        return chatId;
    }

    public String getText() {
        return text;
    }

    public String getUsername() {
        return username;
    }

    public boolean isTrusted() {
        return trusted;
    }
}
