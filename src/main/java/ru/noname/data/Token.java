package ru.noname.data;

public class Token {
    private String token;
    private boolean deleted;

    public Token(String token, boolean deleted) {
        this.token = token;
        this.deleted = deleted;
    }

    public String getToken() {
        return token;
    }

    public boolean isDeleted() {
        return deleted;
    }
}
