package ru.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.noname.dao.TokenDao;
import ru.noname.data.Token;

import java.util.List;

@Controller
@RequestMapping("/")
public class TokenController {
    private TokenDao tokenDao;

    @GetMapping("/tokens")
    public String getTokens(Model model) {
        List<Token> tokens = tokenDao.getTokens();
        model.addAttribute("tokens", tokens);
        return "tokens";
    }

    @GetMapping("/add_token")
    public String addToken() {
        tokenDao.addToken();
        return "redirect:tokens";
    }

    @Autowired
    public void setTokenDao(TokenDao tokenDao) {
        this.tokenDao = tokenDao;
    }
}
