package ru.noname.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ExceptionController {
    @ExceptionHandler(Exception.class)
    public String handle(HttpServletRequest req, Exception e, Model model) {
        model.addAttribute("url", req.getRequestURL());
        model.addAttribute("error", e);
        return "error";
    }
}
