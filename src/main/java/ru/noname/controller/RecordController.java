package ru.noname.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.noname.dao.RecordDao;
import ru.noname.dao.RecordsOrder;
import ru.noname.data.Record;
import ru.noname.data.RecordHelper;
import ru.noname.service.TelegramService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class RecordController extends ExceptionController {
    private static final Logger log = LogManager.getLogger(RecordController.class);
    private RecordDao recordDao;
    private TelegramService telegramService;

    @GetMapping("/list")
    String getRecordList(@RequestParam(name = "by", required = false, defaultValue = "cre") String by,
                         @RequestParam(name = "tag", required = false) String tag,
                         @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                         Model model
    ) {
        final int PAGE_SIZE = 20;
        List<Record> records;
        switch (by.toLowerCase()) {
            case "cre":
                records = recordDao.getRecordsPage(RecordsOrder.BY_CREATED, PAGE_SIZE, page);
                break;
            case "mod":
                records = recordDao.getRecordsPage(RecordsOrder.BY_MODIFIED, PAGE_SIZE, page);
                break;
            case "id":
            default:
                records = recordDao.getRecordsPage(RecordsOrder.BY_ID, PAGE_SIZE, page);
        }
        List<RecordHelper> helpers = new ArrayList<>(records.size());
        for (Record record : records)
            helpers.add(new RecordHelper(record));
        int total_pages = recordDao.getTotalPages(PAGE_SIZE);
        List<Integer> pages = new ArrayList<>();
        pages.add(1);
        for (int i = page - 5; i <= page + 5; i++) {
            if (i > 1 && i < total_pages)
                pages.add(i);
        }
        if (total_pages > 1)
            pages.add(total_pages);
        model.addAttribute("records", helpers);
        model.addAttribute("page", page);
        model.addAttribute("pages", pages);
        return "main";
    }

    @GetMapping("/show")
    String showRecord(
            @RequestParam(name = "id") int id,
            Model model
    ) {
        Record record = recordDao.getRecordById(id);
        if (record != null) {
            model.addAttribute("record", new RecordHelper(record));
            return "show";
        } else {
            log.info("Request record which not exist: id=" + id);
            model.addAttribute("error", "There is no record with id=" + id);
            return "error";
        }
    }

    @GetMapping("/login")
    String getLogin() {
        return "login";
    }

    @GetMapping({"/edit", "/new"})
    String editRecord(@RequestParam(name = "id", required = false) Integer id, Model model) {
        RecordHelper record;
        if (id == null)
            record = new RecordHelper();
        else {
            record = new RecordHelper(recordDao.getRecordById(id));
        }

        model.addAttribute("record", record);
        return "edit";
    }

    @PostMapping("/edit")
    String postEdit(@RequestParam(name = "id", required = false) Integer id,
                    @RequestParam(name = "desc") String description,
                    @RequestParam(name = "tags") String tags,
                    @RequestParam(name = "content") String content,
                    Model model
    ) {
        RecordHelper record = new RecordHelper();
        record.setDescription(description)
                .setContent(content)
                .setTags(tags);

        if (id == null) {
            ZonedDateTime currentTime = ZonedDateTime.now(ZoneId.of("Europe/Moscow"));
            record.setDate(currentTime.toLocalDate());
            record.setTime(currentTime.toLocalTime());
            recordDao.createRecord(record.getRecord());
            log.info("New record");
            telegramService.sendMessage("New record");
        } else {
            record.setId(id);
            recordDao.updateRecord(record.getRecord());
            log.info("Edit record with id #" + id);
            telegramService.sendMessage("Edit record with id #" + id);
        }

        return "redirect:/";
    }

    @GetMapping("/del")
    String delete(@RequestParam(name = "id") int id, Model model) {
        telegramService.sendMessage("Delete record with id #" + id);
        recordDao.deleteRecordById(id);
        return "redirect:/";
    }

    @Autowired
    public void setRecordDao(RecordDao recordDao) {
        this.recordDao = recordDao;
    }

    @Autowired
    public void setTelegramService(TelegramService telegramService) {
        this.telegramService = telegramService;
    }
}
