package ru.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.noname.service.AuthService;
import ru.noname.service.StatsService;

@Controller
@RequestMapping("/")
public class StatsController extends ExceptionController {
    private StatsService statsService;
    private AuthService authService;

    @GetMapping("/stats")
    String getStats(Model model) {
        model.addAttribute("simpleStats", statsService.getSimpleStats());
        model.addAttribute("auths", authService.getAuthDetails());

        return "stats";
    }

    @Autowired
    public void setStatsService(StatsService statsService) {
        this.statsService = statsService;
    }

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }
}
