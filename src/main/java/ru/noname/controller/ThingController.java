package ru.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.noname.dao.ThingDao;
import ru.noname.data.Thing;

import java.time.LocalDate;

@Controller
@RequestMapping("/")
public class ThingController extends ExceptionController {
    private ThingDao thingDao;

    @GetMapping("/done")
    public String done(
            @RequestParam(name = "id") int id
    ) {
        thingDao.setThingCounter(id, 1);
        return "redirect:" + redirectURIForId(id);
    }

    @GetMapping("/undone")
    public String undone(
            @RequestParam(name = "id") int id
    ) {
        thingDao.setThingCounter(id, 0);
        return "redirect:" + redirectURIForId(id);
    }

    @GetMapping("/delThing")
    public String delete(
            @RequestParam(name = "id") int id
    ) {
        String redirectAppend = redirectURIForId(id);
        thingDao.deleteThing(id);
        return "redirect:" + redirectAppend;
    }

    @GetMapping("/today")
    public String moveToToday(
            @RequestParam(name = "id") int id
    ) {
        String redirectAppend = redirectURIForId(id);
        thingDao.moveToToday(id);
        return "redirect:" + redirectAppend;
    }

    @PostMapping("/editThing")
    public String editThing(
            @RequestParam(name = "id") int id,
            @RequestParam(name = "label") String label
    ) {
        String redirectAppend = redirectURIForId(id);
        thingDao.setThingLabel(id, label);
        return "redirect:" + redirectAppend;
    }

    @PostMapping("/addThing")
    public String addThing(
            @RequestParam(name = "label") String label,
            @RequestParam(name = "type") String type,
            @RequestParam(name = "day") int day,
            @RequestParam(name = "month") int month,
            @RequestParam(name = "year") int year
    ) {
        Thing thing = new Thing();
        thing.setLabel(label);
        thing.setType(Thing.Type.valueOf(type));
//        thing.setDay(day);
//        thing.setMonth(month);
//        thing.setYear(year);
        thing.setDate(LocalDate.of(year, month, day));
        String redirectAppend = redirectURI(thing);
        thingDao.createThing(thing);
        return "redirect:" + redirectAppend;
    }

    @PostMapping("/changeCounter")
    public String changeCounter(
            @RequestParam(name = "id") int id,
            @RequestParam(name = "set") String set,
            @RequestParam(name = "inc") String inc
    ) {
        if (set != null && set.length() != 0)
            thingDao.setThingCounter(id, Integer.parseInt(set));
        if (inc != null && inc.length() != 0) {
            Thing thing = thingDao.getThingById(id);
            thingDao.setThingCounter(id, thing.getCounter() + Integer.parseInt(inc));
        }
        return "redirect:" + redirectURIForId(id);
    }

    private String redirectURIForId(int id) {
        Thing thing = thingDao.getThingById(id);
        if (thing != null)
            return redirectURI(thing);
        else
            return "";
    }

    private static String redirectURI(Thing thing) {
        return String.format("day?day=%d&month=%d&year=%d", thing.getDay(), thing.getMonth(), thing.getYear());
    }

    @Autowired
    public void setThingDao(ThingDao thingDao) {
        this.thingDao = thingDao;
    }
}
