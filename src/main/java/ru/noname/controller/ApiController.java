package ru.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ru.noname.dao.RecordDao;
import ru.noname.dao.TokenDao;
import ru.noname.data.Record;


@RestController
@RequestMapping("/api")
public class ApiController {
    private TokenDao tokenDao;
    private RecordDao recordDao;

    static {
        Class<ApiController> clazz = ApiController.class;

    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Record getRecord(@PathVariable("id") int id) {
        return recordDao.getRecordById(id);
    }


    @GetMapping(value = "/{token}/record/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Record getSome(Model model, @PathVariable String token, @PathVariable("id") int id) {
        if (!tokenDao.validate(token))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "{'ok':false}");
        return recordDao.getRecordById(id);
    }

    @Autowired
    public void setRecordDao(RecordDao recordDao) {
        this.recordDao = recordDao;
    }

    @Autowired
    public void setTokenDao(TokenDao tokenDao) {
        this.tokenDao = tokenDao;
    }
}
