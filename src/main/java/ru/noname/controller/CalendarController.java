package ru.noname.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.noname.dao.RecordDao;
import ru.noname.data.RecordHelper;
import ru.noname.service.CalendarService;
import ru.noname.service.DayService;
import ru.noname.service.TelegramService;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Controller
@RequestMapping("/")
public class CalendarController extends ExceptionController {
    private CalendarService calendarService;
    private TelegramService telegramService;
    private DayService dayService;
    private RecordDao recordDao;

    @GetMapping("/")
    public String getCalendar(
            @RequestParam(name = "month", required = false) Integer month,
            @RequestParam(name = "year", required = false) Integer year,
            Model model
    ) {
        LocalDate current = ZonedDateTime.now(ZoneId.of("Europe/Moscow")).toLocalDate();
        if (month == null || month < 1 || month > 12)
            month = current.getMonthValue();
        if (year == null)
            year = current.getYear();
        model.addAttribute("month", calendarService.getMonth(month, year));
        return "calendar";
    }

    @GetMapping("/day")
    public String getDay(
            @RequestParam(name = "day", required = false) Integer day,
            @RequestParam(name = "month", required = false) Integer month,
            @RequestParam(name = "year", required = false) Integer year,
            Model model
    ) {
        LocalDate current = ZonedDateTime.now(ZoneId.of("Europe/Moscow")).toLocalDate();
        if (day == null)
            day = current.getDayOfMonth();
        if (year == null)
            year = current.getYear();
        if (month == null || month < 0 || month > 11)
            month = current.getMonthValue();

        model.addAttribute("day", dayService.getDay(day, month, year));
        return "day";
    }

    @PostMapping("/add")
    public String addRecord(
            @RequestParam(name = "year") int year,
            @RequestParam(name = "month") int month,
            @RequestParam(name = "day") int day,
            @RequestParam(name = "text") String text,
            @RequestParam(name = "time") String time
    ) {
        RecordHelper helper = new RecordHelper();
        helper.setDate(LocalDate.of(year, month, day));
        helper.setTime(LocalTime.parse(time));
        helper.setContent(text);
        helper.setDescription("");
        helper.setTags("");

        recordDao.createRecord(helper.getRecord());
        telegramService.sendMessage("Created record for " + LocalDate.of(year, month, day));
        return String.format("redirect:day?year=%d&month=%d&day=%d", year, month, day);
    }

    @Autowired
    public void setCalendarService(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @Autowired
    public void setTelegramService(TelegramService telegramService) {
        this.telegramService = telegramService;
    }

    @Autowired
    public void setDayService(DayService dayService) {
        this.dayService = dayService;
    }

    @Autowired
    public void setRecordDao(RecordDao recordDao) {
        this.recordDao = recordDao;
    }
}
